const fs = require('fs');

UpdateUser = (fourCard, montoADebitar) => {
	let urlDirectory = __dirname + "/usuarios.json";
	try {
		let stringJson = fs.readFileSync(urlDirectory);
		let jsonToReturn = JSON.parse(stringJson);
		let saldoTotal = 0;

		for (var i = 0; i < jsonToReturn.usuarios.length; i++) {
			console.log(jsonToReturn.usuarios[i])

			if (jsonToReturn.usuarios[i].digitos === fourCard) {
				jsonToReturn.usuarios[i].saldo -= montoADebitar
				saldoTotal = jsonToReturn.usuarios[i].saldo;
			}

		}
		fs.writeFileSync(urlDirectory, JSON.stringify(jsonToReturn))
		return saldoTotal

	} catch(err) {
		console.error(err);
	}
}

exports.default = UpdateUser;