const Mongo = require ('./clases/MongoDB');

(async () => {
	try {
		let mongo = new Mongo ('local','mongodb://localhost:27017');
		let [db,  client] = await mongo.Conectar();
		let docs = await mongo.Find(db);
		mongo.Close(client)
	} catch(e) {
		console.log(e);
	}

})()

