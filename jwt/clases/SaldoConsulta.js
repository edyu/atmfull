const Saldo = require ('./Saldo');
const Mongo = require ('./MongoDB');

class SaldoConsulta extends Saldo {
	constructor() {
		super()
		this.total = 0
		this.cantidad = 0
	}

	GetTotal () {
		return this.total
	}
	GetCantidad (){
		return this.cantidad
	}
	async MostraSaldoConsulta (digitos) {
		let mongo = new Mongo ('local','mongodb://localhost:27017');
		let [db,  client] = await mongo.Conectar();
		let docs = await mongo.Find(db);
		await mongo.Close(client)
		for (let  i = 0; i < docs.length; i++) {
			if (docs[i].digitos == digitos) {
				return docs[i].saldo;
			}
		}
	}


}
module.exports = SaldoConsulta;