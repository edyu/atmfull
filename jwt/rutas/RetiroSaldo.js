const ATM = require ('../clases/ATM');
const Cuenta = require ('../clases/Cuenta');
const SaldoRetiro = require ('../clases/SaldoRetiro');


async function ConsultaDeSaldo (req, res) {
	let digitos = req.body.digitos;
	let retiro = req.body.retiro;
	console.log(digitos);
	let saldoRetiro = new SaldoRetiro();
	let cuenta = new Cuenta(saldoRetiro);
	let atm = new ATM(cuenta)
	let monto = await atm.cuenta.saldo.RetiroSaldo(digitos, retiro);
	res.json({monto : monto });
}

module.exports = ConsultaDeSaldo;