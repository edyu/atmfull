const ATM = require ('../clases/ATM');
const Cuenta = require ('../clases/Cuenta');
const SaldoConsulta = require ('../clases/SaldoConsulta');

async function ConsultaDeSaldo (req, res) {
	let digitos = req.body.digitos;
	console.log(digitos);
	let saldoConsulta = new SaldoConsulta();
	let cuenta = new Cuenta(saldoConsulta);
	let atm = new ATM(cuenta)
	let monto = await atm.cuenta.saldo.MostraSaldoConsulta(digitos);
	res.json({monto : monto });
}

module.exports = ConsultaDeSaldo;