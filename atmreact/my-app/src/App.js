import React ,{Component} from 'react';
import './App.css';
import './index.js';
import './index.css';
import "react-simple-keyboard/build/css/index.css";
import Keyboard from "react-simple-keyboard";


class Inicio extends Component { 
  autenticarUsuario = () => {
    let [cedulaInput] = document.getElementsByClassName("cedula");
    let [digitosInput] = document.getElementsByClassName("digitos");

    let cedula = parseInt(cedulaInput.value);
    let digitos = parseInt(digitosInput.value);

    fetch('http://localhost:3001/autenticar', {
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      method : 'POST',
      body : JSON.stringify({cedula: cedula, digitos: digitos})
      }).then(response => response.json())
    .then((json) => { 
      console.log(json);
      if(json.token) {
        this.props.setToken(json.token);
        this.props.CambiarEstadoAutenticado();
      }
    });
  }

  render() {
    return (
      <div>
        <input className='cedula'></input>
        <input className='digitos'></input>
        <button onClick = {this.autenticarUsuario}/>
      </div>
    ) 
  }
}

class Operaciones extends Component { 
    super(props);
    this.state = {
      input : "",
      screen : "",
      accion : "",
    }
  }

  onKeyPress = (input) => {
    let ea = this.state;
    ea.input += input
    if (ea.input.length === 4) {
      console.log(typeof ea.input)
      let screen = <div><button  className = "Saldo" onClick = {this.consultarSaldo}>Consulta de Fondo</button><button className = "Retiro">Retiro</button></div>
        this.setState({screen:screen})
    }
  }  
  onClick = (e) => {
    let screen = <h1>BIENVENIDO coloque sus ultimos 4 digitos de su tarjeta</h1>
    this.setState({screen:screen})
  }

  consultarSaldo = () => {
    let [digitos] = document.getElementsByClassName("digito");
    console.log(digitos)
    fetch('http://localhost:3001/getPersona', {
      headers: new Headers({
        'Content-Type': 'application/json',
        'access-token': this.props.token,
      }),
      method : 'POST',
      body : JSON.stringify({digitos: digitos.value})
      }).then(response => response.json())
    .then((json) => { 
      let screen = <h1>Su saldo es {json.saldo}</h1>
      this.setState({user: json.data, screen: screen})
    });
  }

  retiro = (e) => {
    let screen = <h1>BIENVENIDO coloque su contraseña</h1>
    this.setState({screen:screen})
  }

  render() {
    return(
    <div className ="App">
        {this.state.screen}

        <input className = "digito" type = "password" value = {this.state.input} readOnly = {true} />
        <button className = "boton" 
        onClick = {this.onClick}>INGRESA TARJETA</button>

          <div className="teclado">
          <Keyboard
            keyboardRef = {r => (this.keyboard = r)}
            onKeyPress = {button => this.onKeyPress(button)}
            theme={"hg-theme-default hg-layout-numeric numeric-theme"}
            layout={{
              default: [
                "1 2 3", "4 5 6", "7 8 9", "Continuar 0 Eliminar", 
              ]
            }}  
          />
        </div>

      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      estaAutenticado: false,
      token: '',
    }
  }

  CambiarEstadoAutenticado = () => {
    this.setState({
      estaAutenticado: true,
    })
  }
  
  setToken = (token) => { 
    this.setState({
      token: token,
    })
  }

  render() {
    if (this.state.estaAutenticado) { 
      return(
        <Operaciones token={this.state.token}/>
      );
    } else {
      return(
        <Inicio CambiarEstadoAutenticado={this.CambiarEstadoAutenticado} setToken={this.setToken}/>
      );
    } 
  }
}
export default App;
