const Saldo = require ("")
class SaldoRetiro extends Saldo {
	constructor(){
		this.monto = 0;
		this.cantidad = 0;
	}
	GetMonto () {
		return this.monto
	}
	GetCantidad(){
		return this.cantidad
	}
	SetMonto (value) {
		this.monto = value
	}
	SetCantidad() {
		this.cantidad = value
	}
 	RetiroSaldo() {
 		return 100;
	}
	VerificarSaldoTotal(){
		return true
	}
}
export default SaldoRetiro;
