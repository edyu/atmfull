const fs = require('fs');

FindUser = (fourCard) => {
	let urlDirectory = __dirname + "/usuarios.json";
	try {
		let stringJson = fs.readFileSync(urlDirectory);
		let jsonToReturn = JSON.parse(stringJson);
		for (let nombreArchivo of jsonToReturn.usuarios) {
				if (nombreArchivo.digitos === fourCard) {
					return nombreArchivo 	
				}
		}
	} catch(err) {
		console.error(err);
	}
}

exports.default = FindUser;