const Express = require ("express");
const app = Express();
const FindUser = require('./findUser').default; 
const bodyParser = require('body-parser');
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});
app.use(bodyParser.json());
app.post('/getPersona', (req, res) => {
	let digitos = req.body.digitos;
	console.log(digitos);
	let jsonPersona = FindUser(digitos)
	res.json(jsonPersona);
});
app.listen (3001, () => console.log('listo el servidor' ));